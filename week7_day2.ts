import { Component, OnInit, ɵɵsetComponentScope } from '@angular/core';
import {FormGroup,FormBuilder,Validators } from '@angular/forms';
@Component({
  selector: 'app-validateform',
  templateUrl: './validateform.component.html',
  styleUrls: ['./validateform.component.css']
})
export class ValidateformComponent implements OnInit {

  registerForm: FormGroup;//object of formgroup with the name of registeredform
  submitted = false;
//this.formBuilder=new FormBuilder
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group
      ({
          title: ['', Validators.required],
          firstName: ['', [Validators.required,Validators.minLength(5)]],
          lastName: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required],
      
         acceptTerms: [false, Validators.requiredTrue]
      /* {
          validator: MustMatch('password', 'confirmPassword')
      }*/
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }//instead of wiriting 
  //this.registredFrom.controls everytime i will just use f

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
console.log("vAlid form");
      // display form values on success
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }
}


